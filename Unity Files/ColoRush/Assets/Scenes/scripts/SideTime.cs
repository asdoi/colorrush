﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SideTime : MonoBehaviour {
public float side;
public Slider slider;
public Text actual;
public InputField field;
bool focused = false;
public GameObject disabled;

	// Use this for initialization
	void Start () {
		side = PlayerPrefs.GetFloat("SideTime");
		slider.value = side;
		field.text = "" + side;
	}
	
	// Update is called once per frame
	void Update () {
		if(PlayerPrefs.GetFloat("SideTime") == 0){
			disabled.SetActive(true);
		}
		else
			disabled.SetActive(false);
		PlayerPrefs.SetFloat("SideTime", side);
		slider.value = side;
		if(!focused)
			field.text = "" + side;
		if (field.isFocused){
			focused = true;
		}
		if(!field.isFocused && focused){
			changeValue();
		}
	}

	public void changeValue(){
		Debug.Log(float.Parse(field.text));
		side = float.Parse(field.text);
		focused = false;
	}

	public void ValueChange(){
		side = slider.value;
	}

	public void Disable(){
		if(PlayerPrefs.GetFloat("SideTime") == 0)
			side = 0.1f;
		else
			side = 0;
	}
}
