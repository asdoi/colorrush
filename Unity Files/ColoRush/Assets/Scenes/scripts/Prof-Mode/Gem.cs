﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : MonoBehaviour {
public GameObject GameController;
public bool tutorial;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "sphere"){
			if(tutorial){
				GameController.GetComponent<TutorialGameController>().score++;
				GameController.GetComponent<TutorialGameController>().GemScore++;
			}
			else{
			GameController.GetComponent<GameControllerProf>().score++;
			GameController.GetComponent<GameControllerProf>().GemScore++;
			}
			gameObject.SetActive(false);
			Destroy(this);
			
		}

	}
}
