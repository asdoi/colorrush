﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class background : MonoBehaviour
{
    public GameObject sphere;
    public GameObject backgrounds;
    int backgroundNR;
    public GameObject[] BackgroundPictures;

    // Start is called before the first frame update
    void Start()
    {
        backgroundNR = PlayerPrefs.GetInt("Backgrounds");
        // backgroundNR = 3;
        BackgroundPictures[backgroundNR].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        backgrounds.transform.position = new Vector3(backgrounds.transform.position.x,backgrounds.transform.position.y,sphere.transform.position.z);
    }
}
