# compiler: R8
# compiler_version: 1.4.77
# min_api: 19
android.support.v4.content.FileProvider -> android.support.v4.content.FileProvider:
    java.io.File DEVICE_ROOT -> b
    java.lang.String[] COLUMNS -> a
    android.support.v4.content.FileProvider$PathStrategy mStrategy -> d
    java.util.HashMap sCache -> c
    java.io.File buildPath(java.io.File,java.lang.String[]) -> a
    java.lang.Object[] copyOf(java.lang.Object[],int) -> a
    java.lang.String[] copyOf(java.lang.String[],int) -> a
    android.support.v4.content.FileProvider$PathStrategy getPathStrategy(android.content.Context,java.lang.String) -> a
    int modeToMode(java.lang.String) -> a
    android.support.v4.content.FileProvider$PathStrategy parsePathStrategy(android.content.Context,java.lang.String) -> b
android.support.v4.content.FileProvider$PathStrategy -> android.support.v4.content.FileProvider$a:
    java.io.File getFileForUri(android.net.Uri) -> a
android.support.v4.content.FileProvider$SimplePathStrategy -> android.support.v4.content.FileProvider$b:
    java.util.HashMap mRoots -> b
    java.lang.String mAuthority -> a
    void addRoot(java.lang.String,java.io.File) -> a
    java.io.File getFileForUri(android.net.Uri) -> a
com.google.androidgamesdk.ChoreographerCallback$1 -> com.google.androidgamesdk.a:
com.google.androidgamesdk.SwappyDisplayManager$1 -> com.google.androidgamesdk.b:
com.google.androidgamesdk.SwappyDisplayManager$a -> com.google.androidgamesdk.SwappyDisplayManager$a:
    com.google.androidgamesdk.SwappyDisplayManager b -> d
    java.util.concurrent.locks.Condition d -> c
    java.util.concurrent.locks.Lock c -> b
com.hippogames.simpleandroidnotifications.AppIconHelper -> com.hippogames.simpleandroidnotifications.a:
    android.graphics.Bitmap GetAppIcon(android.graphics.drawable.Drawable) -> a
    android.graphics.Bitmap MergeAppIcon26(android.graphics.drawable.Drawable) -> b
com.hippogames.simpleandroidnotifications.Controller -> com.hippogames.simpleandroidnotifications.Controller:
    android.app.Notification BuildNotification(android.content.Context,com.hippogames.simpleandroidnotifications.NotificationParams,android.content.Intent,android.app.NotificationManager) -> a
    void CreateStackedNotification(android.content.Context,android.content.Intent,com.hippogames.simpleandroidnotifications.NotificationParams) -> a
    boolean IsEmpty(java.lang.String) -> a
    void SetLargeIcon(java.lang.String,android.app.Notification$Builder,android.content.Context) -> a
com.hippogames.simpleandroidnotifications.NotificationParams -> com.hippogames.simpleandroidnotifications.b:
    java.lang.String Title -> g
    boolean Vibrate -> m
    java.lang.String Ticker -> i
    boolean Light -> o
    java.lang.String Message -> h
    boolean Multiline -> j
    boolean Sound -> k
    java.lang.String CustomSound -> l
    long[] Vibration -> n
    java.lang.String LargeIcon -> s
    int Id -> a
    java.lang.String SmallIcon -> t
    java.lang.String CallbackData -> w
    java.lang.String UnityClass -> x
    int Importance -> v
    boolean Repeat -> f
    int SmallIconColor -> u
    java.lang.String GroupSummary -> c
    int LightColor -> r
    java.lang.String GroupName -> b
    java.lang.String ChannelName -> e
    int LightOnMs -> p
    java.lang.String ChannelId -> d
    int LightOffMs -> q
com.hippogames.simpleandroidnotifications.Serializer -> com.hippogames.simpleandroidnotifications.c:
    java.lang.Object FromString(java.lang.String) -> a
com.hippogames.simpleandroidnotifications.Storage -> com.hippogames.simpleandroidnotifications.d:
    com.hippogames.simpleandroidnotifications.NotificationParams GetNotification(android.content.Context,int) -> a
    java.util.List GetNotificationIds(android.content.Context) -> a
    void RemoveNotification(android.content.Context,int) -> b
com.techtweaking.a.a -> a.a.a.a:
    byte[] a -> b
    byte b -> k
    java.util.LinkedList a -> j
    int t -> i
    int s -> f
    byte[] buf -> h
    int a -> g
    int q -> d
    int r -> e
    int p -> c
    int o -> a
    int size() -> a
com.techtweaking.a.b -> a.a.a.b:
    int[] c -> a
com.techtweaking.a.c -> a.a.a.c:
    int v -> b
    int w -> c
    int u -> a
    int[] b() -> a
com.techtweaking.bluetoothcontroller.BluetoothConnection -> com.techtweaking.bluetoothcontroller.a:
    int b -> o
    int a -> j
    boolean f -> u
    java.lang.String a -> q
    java.lang.String name -> p
    int id -> b
    java.io.OutputStream a -> l
    java.lang.String b -> r
    byte a -> i
    java.io.InputStream a -> n
    android.bluetooth.BluetoothSocket a -> k
    android.bluetooth.BluetoothDevice a -> s
    boolean b -> e
    int d -> v
    java.lang.Object a -> c
    boolean c -> f
    boolean d -> g
    int c -> t
    boolean e -> h
    java.io.BufferedOutputStream a -> m
    boolean a -> d
    void b() -> a
    void close() -> b
    void d() -> c
    void f() -> d
    void g() -> e
    java.lang.String getAddress() -> f
    int getID() -> g
    java.lang.String getName() -> h
    void c() -> i
    void e() -> j
com.techtweaking.bluetoothcontroller.ForwardingActivity -> com.techtweaking.bluetoothcontroller.ForwardingActivity:
    boolean q -> a
    int n -> b
com.techtweaking.bluetoothcontroller.a -> com.techtweaking.bluetoothcontroller.b:
    int[] a -> f
    int h -> d
    int i -> e
    int f -> b
    int g -> c
    int e -> a
com.techtweaking.bluetoothcontroller.b -> com.techtweaking.bluetoothcontroller.c:
    com.techtweaking.bluetoothcontroller.BluetoothConnection a -> i
    java.util.UUID uuid -> c
    boolean h -> e
    android.bluetooth.BluetoothDevice a -> h
    boolean i -> f
    boolean j -> g
    int k -> b
    int j -> a
    boolean g -> d
com.techtweaking.bluetoothcontroller.e -> com.techtweaking.bluetoothcontroller.d:
    java.util.Queue a -> f
    java.lang.Object b -> h
    android.content.BroadcastReceiver a -> i
    com.techtweaking.bluetoothcontroller.j a -> c
    android.util.SparseArray a -> g
    android.bluetooth.BluetoothAdapter a -> b
    com.techtweaking.bluetoothcontroller.b a -> e
    com.techtweaking.bluetoothcontroller.h a -> j
    boolean k -> d
com.techtweaking.bluetoothcontroller.f -> com.techtweaking.bluetoothcontroller.e:
    com.techtweaking.bluetoothcontroller.e b -> a
com.techtweaking.bluetoothcontroller.g -> com.techtweaking.bluetoothcontroller.f:
    int[] b -> a
com.techtweaking.bluetoothcontroller.h -> com.techtweaking.bluetoothcontroller.g:
com.techtweaking.bluetoothcontroller.i -> com.techtweaking.bluetoothcontroller.h:
    com.techtweaking.bluetoothcontroller.e b -> a
com.techtweaking.bluetoothcontroller.j -> com.techtweaking.bluetoothcontroller.i:
    com.techtweaking.bluetoothcontroller.e b -> a
com.techtweaking.bluetoothcontroller.m -> com.techtweaking.bluetoothcontroller.j:
    byte[] a -> b
com.techtweaking.bluetoothcontroller.n -> com.techtweaking.bluetoothcontroller.k:
    android.bluetooth.BluetoothSocket a -> b
    com.techtweaking.a.a a -> g
    java.lang.Object c -> d
    boolean n -> f
    java.io.InputStream a -> c
    int id -> e
    boolean m -> a
    void setPacketSize(int) -> a
    void i() -> b
    void setEndByte(byte) -> b
com.techtweaking.bluetoothcontroller.o -> com.techtweaking.bluetoothcontroller.l:
com.techtweaking.bluetoothcontroller.p -> com.techtweaking.bluetoothcontroller.m:
    android.util.SparseArray b -> c
    com.techtweaking.bluetoothcontroller.m b -> e
    boolean o -> a
    int m -> b
    int b() -> a
    void j() -> b
    void k() -> c
    boolean a() -> d
com.techtweaking.bluetoothcontroller.q -> com.techtweaking.bluetoothcontroller.n:
    android.util.SparseArray c -> a
    java.lang.Object d -> b
com.techtweaking.bluetoothcontroller.s -> com.techtweaking.bluetoothcontroller.o:
com.techtweaking.bluetoothcontroller.w -> com.techtweaking.bluetoothcontroller.p:
    java.lang.String c -> a
com.techtweaking.bluetoothcontroller.x -> com.techtweaking.bluetoothcontroller.q:
    com.techtweaking.bluetoothcontroller.x[] a -> s
    java.lang.String value -> t
    void c(java.lang.String) -> a
    void send(int) -> a
