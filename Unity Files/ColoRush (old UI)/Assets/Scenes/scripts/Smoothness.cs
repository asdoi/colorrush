﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Smoothness : MonoBehaviour {
public float smooth;
public Slider slider;
public Text actual;
public InputField field;
bool focused = false;
public GameObject disabled;

	// Use this for initialization
	void Start () {
		smooth = PlayerPrefs.GetFloat("Smooth");
		slider.value = smooth;
		field.text = "" + smooth;
	}
	
	// Update is called once per frame
	void Update () {
		if(PlayerPrefs.GetInt("SmoothOn") == 0){
			disabled.SetActive(true);
		}
		else
			disabled.SetActive(false);
		PlayerPrefs.SetFloat("Smooth", smooth);
		slider.value = smooth;
		if(!focused)
			field.text = "" + smooth;
		if (field.isFocused){
			focused = true;
		}
		if(!field.isFocused && focused){
			changeValue();
		}
	}

	public void changeValue(){
		Debug.Log(float.Parse(field.text));
		smooth = float.Parse(field.text);
		focused = false;
		PlayerPrefs.SetInt("SmoothOn",1);
	}

	public void ValueChange(){
		smooth = slider.value;
		PlayerPrefs.SetInt("SmoothOn",1);
	}

	public void Disable(){
		if(PlayerPrefs.GetInt("SmoothOn") == 1)
			PlayerPrefs.SetInt("SmoothOn",0);
		else
			PlayerPrefs.SetInt("SmoothOn",1);
	}
}
