﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereEndless : MonoBehaviour {
public GameObject GameController;
public string color;
public GameObject[] Skins;

	// Use this for initialization
	void Start () {
		Skins[GameController.GetComponent<GameController>().SkinNum].SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < Skins.Length; i++)
		{
			Skins[i].SetActive(false);
		}
		Skins[GameController.GetComponent<GameController>().SkinNum].SetActive(true);
		for (int i = 0; i < Skins.Length; i++)
		{		
			Skins[i].GetComponent<Skins>().changeColor(color);	
		}
	}

	public void red(){
		color = "red";
	}
	
	public void blue(){
		color = "blue";
	}

	public void yellow(){
		color = "yellow";
	}

	public void doubleColor(string C2){
		Debug.Log("doubleColor");
		switch (C2)
		{
			case "blue":
				switch (color)
				{	
					case "red":
						color = "purple";
						break;
					case "yellow":
						color = "green";
						break;
					default:
						color = "blue";
						break;
				}
				break;
			case "red":
				if(color == "yellow")
					color = "orange";
				else if(color == "blue")
					color = "purple";
				else
					color = "red";
				break;
			case "yellow":
				if(color == "red")
					color = "orange";
				else if(color == "blue")
					color = "green";
				else
					color = "yellow";
				break;
			default:
				color = C2;
				break;
		}
	}
}
