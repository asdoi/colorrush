﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrier : MonoBehaviour {
public GameObject sphere;
public GameObject GameController;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "sphere"){
		if(this.name == sphere.GetComponent<SphereEndless>().color){
			Debug.Log("destroy... " + name);
			GameController.GetComponent<GameController>().ScorePlus();
			Destroy(this);
		}
		else
		{
			Debug.Log("over " + name);
			GameController.GetComponent<GameController>().gameOver();
		}
		}

	}
}
