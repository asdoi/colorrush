﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemsBoss : MonoBehaviour {
public GameObject GameController;
public bool Choosing;
public bool Daily;
public bool Multi;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other)
	{	
		if(Choosing && !Daily){
			if(other.gameObject.tag == "sphere"){
				GameController.GetComponent<GameControllerChoose>().score++;
				GameController.GetComponent<GameControllerChoose>().GemScore++;
				gameObject.SetActive(false);
				Destroy(this);
				
			}
		}
		else if(Daily){
			if(other.gameObject.tag == "sphere"){
				GameController.GetComponent<GameControllerDy>().score++;
				GameController.GetComponent<GameControllerDy>().GemScore++;
				gameObject.SetActive(false);
				Destroy(this);
				
			}
		}
		else if(Multi){
			if(other.gameObject.tag == "sphere"){
				GameController.GetComponent<GameControllerMultiplayer>().score++;
				GameController.GetComponent<GameControllerMultiplayer>().GemScore++;
				gameObject.SetActive(false);
				Destroy(this);
				
			}
		}
		else{
			if(other.gameObject.tag == "sphere"){
				GameController.GetComponent<GameControllerBoss>().score++;
				GameController.GetComponent<GameControllerBoss>().GemScore++;
				gameObject.SetActive(false);
				Destroy(this);
				
			}
		}
	}
}
