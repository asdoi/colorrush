﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeOff : MonoBehaviour
{
    public GameObject SideTime;

    public GameObject active;
    public GameObject deactive;
    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.GetInt("SwipeOK") == 1)
            PlayerPrefs.SetFloat("SideTime",0);
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerPrefs.GetInt("SwipeOK") == 0){
            active.SetActive(true);
            deactive.SetActive(false);}
        else{
            active.SetActive(false);
            deactive.SetActive(true);}
        Debug.Log(PlayerPrefs.GetInt("SwipeOK"));
    }

    public void changeSwipe(){
        if(PlayerPrefs.GetInt("SwipeOK") == 0){
            PlayerPrefs.SetInt("SwipeOK",1);
            PlayerPrefs.SetFloat("SideTime",0);
            SideTime.GetComponent<SideTime>().side = 0;}
        else{
            PlayerPrefs.SetInt("SwipeOK",0);
            if(PlayerPrefs.GetFloat("SideTime") == 0){
                PlayerPrefs.SetFloat("SideTime",0.1f);
                SideTime.GetComponent<SideTime>().side = 0.1f;
            }
            }

    }
}
