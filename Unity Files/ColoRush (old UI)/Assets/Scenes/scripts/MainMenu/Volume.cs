﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Volume : MonoBehaviour {
public AudioSource TitleMusic;
	// Use this for initialization
	void Start () {
		TitleMusic.Play(0);
	}
	
	// Update is called once per frame
	void Update () {
		
		Audio();
		RevertChoose();
	}
	public void Mute(){
		int vol = PlayerPrefs.GetInt("Volume");
		if(vol - 1 == 0)
			vol = 0;
		else
			vol = 1;
		PlayerPrefs.SetInt("Volume", vol);
	}

	public void Audio(){
		int vol = PlayerPrefs.GetInt("Volume");
		if(vol == 0)
			TitleMusic.GetComponent<AudioSource>().mute = true;
		else
			TitleMusic.GetComponent<AudioSource>().mute = false;
	}

	public void RevertChoose(){
		PlayerPrefs.SetInt("chosen", 0);
	}
}
