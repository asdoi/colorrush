﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicChoose : MonoBehaviour {
public int AudioNR;
public bool Onleft;
public bool down;

public GameObject leftDown;
public GameObject rightDown;
public GameObject leftUp;
public GameObject rightUp;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		int SkinNr = AudioNR;
		int skin = PlayerPrefs.GetInt("AudioFile");
		if(skin == SkinNr){
			Debug.Log(name);
			if(Onleft && down){
				leftDown.SetActive(true);
				}
			else if(down){
				rightDown.SetActive(true);
			}
			else if(Onleft){
				leftUp.SetActive(true);
			}
			else{
				rightUp.SetActive(true);
				}
		}
		else{
			if(Onleft && down){
				leftDown.SetActive(false);
				}
			else if(down){
				rightDown.SetActive(false);
			}
			else if(Onleft){
				leftUp.SetActive(false);
			}
			else{
				rightUp.SetActive(false);
				}
		}
	}

	public void SetMusic(){
		PlayerPrefs.SetInt("AudioFile", AudioNR);
	}
}
